# pitemp-and-humidity
Python script to read temperatures from an AM2302 temperature and humidity sensor and feed them into the Icinga monitoring system.

## Preparation
Before we can install the python DHT library, there are a few dependencies that need installing.
```bash
$ sudo apt-get update
$ sudo apt-get upgrade
$ sudo apt-get install build-essential python-dev
```
# Install some dependencies
We need a yaml parser for the configuration file:
```bash
$ sudo pip install ruamel.yaml
```

## Setup the Adafruit library
Make sure the Adafruit library for this sensor is installed:
```bash
$ cd ~/src
$ git clone https://github.com/adafruit/Adafruit_Python_DHT.git
$ cd Adafruit_Python_DHT
$ sudo python setup.py install
```
## Test the library
There's a script in the examples directory that reads the sensor and output the 
temperature and relative humidity:
```
$ cd examples
$ ./AdafruitDHT.py 2302 4
Temp=24.8*  Humidity=45.6%
```
This is what it looks like if something goes wrong (after a rather long timeout):
```
$ ./AdafruitDHT.py 2302 5
Failed to get reading. Try again!
```
## Install the check_mk agent plugin
Clone this repository:
```
$ cd ~/src
$ git clone https://gitlab.science.ru.nl/bram/pitemp-and-humidity.git
```
## Configure
Copy the example config file to `config.yml`:
```bash
$ cp config_example.yml config.yml
```
And adjust values for location and the alarm limits.

> Make sure to not use unicode symbols in the configuration file. Although the
> test will run fine. When used as a check_mk agent plugin, the script will
> crash when trying to output the data


## Test
Run the script!
```bash
$ ./check_mk_2302.py 
0 AM23202_temperature temperature_in_celsius=24.500000;18.0:25.0;15.0:30.0;0.0;40.0 24.5*C
0 AM23202_humidity relative_humidity=34.500000;20.0:80.0;10.0:90.0;0.0;100.0 34.5%
```
Change GPIO pin, thresholds in the script.

## Add to check_mk agent
> A fully working check_mk agnt is assumed here

Make sure the script is symlinked in `/usr/lib/check_mk_agent/local`, e.g.:
```bash
pi@raspberrypi:~ $ sudo ln -s ~/src/pitemp-and-humidity/check_mk_2302.py /usr/lib/check_mk_agent/local/
```

Now check_mk_agent will output the temperature and humidity in the `<<<local>>>` section:
```terminal
me@monitoringhost:~$ telnet sensorhost 6556
Trying 1.2.3.4...
Connected to sensorhost
Escape character is '^]'.
<<<check_mk>>>
Version: 1.2.6p12
AgentOS: linux
AgentDirectory: /etc/check_mk
...
...
btime 1530603153
processes 3485
procs_running 1
procs_blocked 0
softirq 935911 79944 256793 1140 57541 0 0 92890 233765 0 213838
<<<md>>>
<<<vbox_guest>>>
<<<lnx_thermal>>>
thermal_zone0 enabled cpu-thermal 49388 
<<<local>>>
0 server_room_temperature temperature=25.100000;15.0:27.0;12.0:30.0;0.0;40.0 25.1*C
0 server_room_humidity humidity=48.500000;35.0:75.0;30.0:80.0;0.0;100.0 48.5%
Connection closed by foreign host.
```