#!/usr/bin/env python

import sys
import os
import time
from ruamel.yaml import YAML
try:
    import Adafruit_DHT
    testmode = False
except:
    sys.stderr.write("WARNING: Adafruit DHT library not found. Continuing in test mode...\n")
    testmode = True

STATUS_OK=0
STATUS_WARNING=1
STATUS_CRITICAL=2
STATUS_UNKNOWN=3

try:
    stream = open(os.path.join(sys.path[0], 'config.yml'), "r")
    yaml = YAML()
    config = yaml.load(stream)
except:
    sys.stderr.write("Unable to read config.yml\n")
    sys.exit(1)

reading = {}

if not testmode:
    # Try to grab a sensor reading.  Use the read_retry method which will retry up
    # to 15 times to get a sensor reading (waiting 2 seconds between each retry).
    reading['humidity'], reading['temperature'] = Adafruit_DHT.read_retry(Adafruit_DHT.AM2302, config['gpioport'], 3, 2)
else:
    # generate a reading
    reading['humidity'] = 5 + (time.time() / 20) % 90
    reading['temperature'] = 10 + (30 + time.time() / 30) % 30


for unit in reading:
    value = reading[unit]


    if value > config[unit]['devmax'] or value < config[unit]['devmin']:
        value = None

    # test for plausible value
    if value is None:
        print "%s %s_%s %s=NaN Invalid reading"  % (STATUS_UNKNOWN, config['location'], unit, unit)
        continue

    metric = "%.1f%s" % (value, config[unit]['symbol'])

    # initial status and message
    status = STATUS_OK
    msg = metric

    range = '%.1f:%.1f;%.1f:%.1f;%.1f;%.1f' % (config[unit]['lowwarn'], config[unit]['highwarn'], config[unit]['lowcrit'], config[unit]['highcrit'], config[unit]['min'], config[unit]['max'])

    if value < config[unit]['lowwarn']:
        status = STATUS_WARNING
        msg = "low %s, %s" % (config[unit]['name'], metric)

    if value < config[unit]['lowcrit']:
        status = STATUS_CRITICAL
        msg = "low %s, %s" % (config[unit]['name'], metric)

    if value > config[unit]['highwarn']:
        status = STATUS_WARNING
        msg = "high %s, %s" % (config[unit]['name'], metric)

    if value > config[unit]['highcrit']:
        status = STATUS_CRITICAL
        msg = "high %s, %s" % (config[unit]['name'], metric)


    # output according to https://mathias-kettner.de/checkmk_localchecks.html#How%20to%20write%20local%20checks
    print "%s %s_%s %s=%f;%s %s" % (status, config['location'], unit, unit, value, range, msg)
